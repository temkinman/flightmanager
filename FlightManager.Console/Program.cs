﻿using System;
using FlightManager.Lib.Core;
using FlightManager.Lib.Exceptions;
using FlightManager.Lib.Model;

namespace FlightManager.ConsoleProj
{
	internal class Program
	{
		private static readonly TicketSaleManager TicketSaleManager = new TicketSaleManager();
		private static readonly PassengerManager PassengerManager = new PassengerManager();
		private static readonly Lib.Core.SimpleFlightManager SimpleFlightManager = new Lib.Core.SimpleFlightManager();

		static void Main(string[] args)
		{
			Init();
			ShowMenu();
		}

		private static void Init()
		{
			foreach (var passenger in DataFiller.CreatePassengers())
			{
				PassengerManager.Create(passenger);
			}

			foreach (var flight in DataFiller.CreateFlights())
			{
				SimpleFlightManager.Create(flight);
			}
		}

		private static void ShowMenu()
		{
			Console.WriteLine();
			Console.WriteLine("1. Найти пассажира");
			Console.WriteLine("2. Найти рейс");
			Console.WriteLine("3. Продать билет");
			Console.WriteLine("4. Вернуть билет");
			Console.WriteLine("5. Удалить всех пассажиров");
			Console.WriteLine("6. Продать билет на опредленное место");
			Console.WriteLine("0. Выход");

			HandleMenu();
		}

		private static void HandleMenu()
		{
			ConsoleKey key = Console.ReadKey().Key;
			
			switch (key)
			{
				case ConsoleKey.D1:
					{
						FindPassenger();
						break;
					}
				case ConsoleKey.D2:
					{
						FindFlight();
						break;
					}
				case ConsoleKey.D3:
					{
						SellTicket();
						break;
					}
				case ConsoleKey.D4:
					{
						RefundTicket();
						break;
					}
				case ConsoleKey.D5:
					{
						PassengerManager.DeleteAll();
						break;
					}
				case ConsoleKey.D6:
				{
					SellTicketWithSeatNumber();
					break;
				}
				case ConsoleKey.D0:
					{
						Environment.Exit(0);
						break;
					}
			}

			ShowMenu();
		}

		private static Passenger FindPassenger()
		{
			Console.WriteLine("\nВведите номер пасспорта:");

			string passNumber = Console.ReadLine();
			Passenger passenger = PassengerManager.Find(passNumber);

			if (passenger != null)
			{
				Console.WriteLine(passenger);
			}
			else
			{
				Console.WriteLine("Пассажир не найден");
			}

			return passenger;
		}

		private static Flight FindFlight()
		{
			Console.WriteLine("\nВведите порт прибытия:");

			string arrivalPort = Console.ReadLine();
			Flight flight = SimpleFlightManager.Find(arrivalPort);

			if (flight != null)
			{
				Console.WriteLine(flight);
			}
			else
			{
				Console.WriteLine("Пассажир не найден");
			}

			return flight;
		}

		private static void SellTicket()
		{
			Passenger passenger = FindPassenger();
			Flight flight = FindFlight();

			TicketSaleManager.SellTicket(flight, passenger);

			Console.WriteLine("Билет куплен!");
		}

		private static void SellTicketWithSeatNumber()
		{
			Passenger passenger = FindPassenger();
			Flight flight = FindFlight();

			Console.WriteLine("Введите номер места:\n");
			int seatNumber = int.Parse(Console.ReadLine());

			try
			{
				TicketSaleManager.SellTicket(flight, passenger, seatNumber);
			}
			catch (ArgumentException argumentException)
			{
				Console.WriteLine($"Введен неверный номер места!\n{argumentException.StackTrace}");
			}
			catch(SeatIsAssignedException seatIsAssignedException)
			{
				// log(seatIsAssignedException.Message)
				Console.WriteLine("Место уже занято!");
			}
		}

		private static void RefundTicket()
		{
			Passenger passenger = FindPassenger();
			Flight flight = FindFlight();

			TicketSaleManager.RefundTicket(flight, passenger);
			Console.WriteLine("Билет возвращен!");
		}
	}
}
