﻿using System.Collections.Generic;
using FlightManager.Lib.Abstract;
using FlightManager.Lib.Model;
using System.Linq;

namespace FlightManager.Lib.Core
{
	public class PassengerManager : IPassengerManager
	{
		public void Create(Passenger passenger)
		{
			DataStorage.Passengers.Add(passenger);
		}

		public void Delete(Passenger passenger)
		{
			DataStorage.Passengers.Remove(passenger);
		}

		public void DeleteAll()
		{
			DataStorage.Passengers.Clear();
		}

		public List<Passenger> GetAll()
		{
			return DataStorage.Passengers;
		}

		public Passenger Find(string passportNumber)
		{
			return DataStorage.Passengers.FirstOrDefault(x => x.PassportNumber == passportNumber);
		}

		public Passenger Find(string firstName, string lastName)
		{
			return DataStorage.Passengers
				.FirstOrDefault(x => x.FirstName.ToLower() == firstName.ToLower()
								  && x.LastName.ToLower() == lastName.ToLower());
		}
	}
}
