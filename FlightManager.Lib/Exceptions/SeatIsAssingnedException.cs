﻿using System;

namespace FlightManager.Lib.Exceptions
{
	public class SeatIsAssignedException : Exception
	{
		public SeatIsAssignedException()
		{
		}

		public SeatIsAssignedException(string message)
			: base(message)
		{
		}

		public SeatIsAssignedException(string message, Exception inner)
			: base(message, inner)
		{
		}
    }
}
