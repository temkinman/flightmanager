﻿using System;

namespace FlightManager.Lib.Model
{
	public class Passenger
	{
		public string PassportNumber { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public DateTime IssueDate { get; set; }
		public string IssuePlace { get; set; }
		public DateTime BirthDate { get; set; }

		public override string ToString()
		{
			return $"{FirstName} {LastName} {BirthDate:d}";
		}
	}
}
