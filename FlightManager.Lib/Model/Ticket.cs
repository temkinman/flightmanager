﻿using System;

namespace FlightManager.Lib.Model
{
	public class Ticket
	{
		public Guid Id { get; set; } = Guid.NewGuid();
		public string PassengerFirstName { get; set; }
		public string PassengerLastName { get; set; }
		public int SeatNumber { get; set; }
	}
}
