﻿using FlightManager.Lib.Core;
using FlightManager.Lib.Model;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FlightManager.LibTests.Core
{
	[TestFixture]
	public class TicketSellManagerTests
	{
		[Test]
		public void TicketSaleManagerShouldCreateTicketWithAutoAssignedSeatNumber()
		{
			TicketSaleManager ticketSaleManager = new TicketSaleManager();
			Flight flight = new Flight
			{
				SeatsNumber = 10
			};
			Passenger passenger = new Passenger
			{
				FirstName = "FirstName",
				LastName = "LastName"
			};
			Ticket ticket = new Ticket
			{
				PassengerFirstName = passenger.FirstName,
				PassengerLastName = passenger.LastName,
				SeatNumber = 1
			};

			ticketSaleManager.SellTicket(flight, passenger);

			Ticket resultTicket = DataStorage.ReservedTickets[flight].First();
			Assert.That(ticket.PassengerFirstName, Is.EqualTo(resultTicket.PassengerFirstName));
			Assert.That(ticket.PassengerLastName, Is.EqualTo(resultTicket.PassengerLastName));
			Assert.That(ticket.SeatNumber, Is.EqualTo(resultTicket.SeatNumber));
		}

		[Test]
		public void TicketSaleManagerShouldCreateTicketWithManualAssignedSeatNumber()
		{
			TicketSaleManager ticketSaleManager = new TicketSaleManager();
			Flight flight = new Flight
			{
				SeatsNumber = 10
			};
			Passenger passenger = new Passenger
			{
				FirstName = "FirstName",
				LastName = "LastName"
			};
			Ticket ticket = new Ticket
			{
				PassengerFirstName = passenger.FirstName,
				PassengerLastName = passenger.LastName,
				SeatNumber = 5
			};

			ticketSaleManager.SellTicket(flight, passenger, 5);

			Ticket resultTicket = DataStorage.ReservedTickets[flight].First();
			Assert.That(ticket.PassengerFirstName, Is.EqualTo(resultTicket.PassengerFirstName));
			Assert.That(ticket.PassengerLastName, Is.EqualTo(resultTicket.PassengerLastName));
			Assert.That(ticket.SeatNumber, Is.EqualTo(resultTicket.SeatNumber));
		}

		[Test]
		public void SellTicketMethodShouldThrowArgumentExceptionWhenInputSeatNumberIsGreaterThanSeatsCount()
		{
			TicketSaleManager ticketSaleManager = new TicketSaleManager();
			Flight flight = new Flight
			{
				SeatsNumber = 10
			};
			Passenger passenger = new Passenger();

			Assert.Throws(
				Is.TypeOf<ArgumentException>()
					.And.Message.EqualTo("Недопустимый номер места! (Parameter 'seatNumber')"),
				() => ticketSaleManager.SellTicket(flight, passenger, 20));
		}

		[Test]
		public void SellTicketMethodShouldThrowInvalidOperationExceptionWhenSeatNumberIsAssigned()
		{
			int assignedSeatNumber = 5;
			TicketSaleManager ticketSaleManager = new TicketSaleManager();
			Flight flight = new Flight
			{
				SeatsNumber = 10
			};
			Passenger passenger = new Passenger();

			DataStorage.ReservedTickets[flight] = new List<Ticket>
			{
				new Ticket
				{
					SeatNumber = assignedSeatNumber
				}
			};

			Assert.Throws(
				Is.TypeOf<InvalidOperationException>()
					.And.Message.EqualTo("Место уже занято!"),
				() => ticketSaleManager.SellTicket(flight, passenger, assignedSeatNumber));
		}
	}
}
