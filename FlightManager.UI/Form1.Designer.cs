﻿namespace FlightManager.UI
{
	partial class Form1
	{
		/// <summary>
		///  Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		///  Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		///  Required method for Designer support - do not modify
		///  the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
			this.FindByPassport = new System.Windows.Forms.Button();
			this.passportNumberBox = new System.Windows.Forms.TextBox();
			this.passangerLabel = new System.Windows.Forms.Label();
			this.passengersDgView = new System.Windows.Forms.DataGridView();
			this.flightsDgView = new System.Windows.Forms.DataGridView();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.LstNameLbl = new System.Windows.Forms.Label();
			this.FstNameLbl = new System.Windows.Forms.Label();
			this.passportLbl = new System.Windows.Forms.Label();
			this.FindByNameBtn = new System.Windows.Forms.Button();
			this.LstNameBox = new System.Windows.Forms.TextBox();
			this.FstNameBox = new System.Windows.Forms.TextBox();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.arrivalPortLbl = new System.Windows.Forms.Label();
			this.flightIdLbl = new System.Windows.Forms.Label();
			this.arrivalPortBtn = new System.Windows.Forms.Button();
			this.flightIdBtn = new System.Windows.Forms.Button();
			this.arrivalPortBox = new System.Windows.Forms.TextBox();
			this.flightIdBox = new System.Windows.Forms.TextBox();
			this.passengerInfoLbl = new System.Windows.Forms.Label();
			this.flightInfoLbl = new System.Windows.Forms.Label();
			this.sellTicketBtn = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this.passengersDgView)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.flightsDgView)).BeginInit();
			this.groupBox1.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.SuspendLayout();
			// 
			// FindByPassport
			// 
			this.FindByPassport.Location = new System.Drawing.Point(333, 25);
			this.FindByPassport.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
			this.FindByPassport.Name = "FindByPassport";
			this.FindByPassport.Size = new System.Drawing.Size(85, 33);
			this.FindByPassport.TabIndex = 0;
			this.FindByPassport.Text = "Find";
			this.FindByPassport.UseVisualStyleBackColor = true;
			this.FindByPassport.Click += new System.EventHandler(this.button1_Click);
			// 
			// passportNumberBox
			// 
			this.passportNumberBox.Location = new System.Drawing.Point(105, 28);
			this.passportNumberBox.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
			this.passportNumberBox.Name = "passportNumberBox";
			this.passportNumberBox.Size = new System.Drawing.Size(225, 27);
			this.passportNumberBox.TabIndex = 1;
			// 
			// passangerLabel
			// 
			this.passangerLabel.AutoSize = true;
			this.passangerLabel.Location = new System.Drawing.Point(9, 95);
			this.passangerLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.passangerLabel.Name = "passangerLabel";
			this.passangerLabel.Size = new System.Drawing.Size(0, 20);
			this.passangerLabel.TabIndex = 2;
			// 
			// passengersDgView
			// 
			dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
			dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
			dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
			dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
			this.passengersDgView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.passengersDgView.ColumnHeadersHeight = 29;
			dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
			dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
			dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
			dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
			this.passengersDgView.DefaultCellStyle = dataGridViewCellStyle2;
			this.passengersDgView.Location = new System.Drawing.Point(14, 228);
			this.passengersDgView.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.passengersDgView.Name = "passengersDgView";
			dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
			dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
			dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
			dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
			this.passengersDgView.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
			this.passengersDgView.RowHeadersVisible = false;
			this.passengersDgView.RowHeadersWidth = 51;
			this.passengersDgView.RowTemplate.Height = 25;
			this.passengersDgView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.passengersDgView.Size = new System.Drawing.Size(426, 336);
			this.passengersDgView.TabIndex = 3;
			// 
			// flightsDgView
			// 
			dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
			dataGridViewCellStyle4.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
			dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
			dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
			this.flightsDgView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
			this.flightsDgView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
			dataGridViewCellStyle5.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
			dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
			dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
			this.flightsDgView.DefaultCellStyle = dataGridViewCellStyle5;
			this.flightsDgView.Location = new System.Drawing.Point(478, 228);
			this.flightsDgView.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.flightsDgView.Name = "flightsDgView";
			dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
			dataGridViewCellStyle6.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
			dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
			dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
			this.flightsDgView.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
			this.flightsDgView.RowHeadersVisible = false;
			this.flightsDgView.RowHeadersWidth = 51;
			this.flightsDgView.RowTemplate.Height = 25;
			this.flightsDgView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.flightsDgView.Size = new System.Drawing.Size(602, 336);
			this.flightsDgView.TabIndex = 4;
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.LstNameLbl);
			this.groupBox1.Controls.Add(this.FstNameLbl);
			this.groupBox1.Controls.Add(this.passportLbl);
			this.groupBox1.Controls.Add(this.FindByNameBtn);
			this.groupBox1.Controls.Add(this.FindByPassport);
			this.groupBox1.Controls.Add(this.LstNameBox);
			this.groupBox1.Controls.Add(this.FstNameBox);
			this.groupBox1.Controls.Add(this.passportNumberBox);
			this.groupBox1.Location = new System.Drawing.Point(15, 16);
			this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.groupBox1.Size = new System.Drawing.Size(425, 184);
			this.groupBox1.TabIndex = 5;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Searching passenger";
			// 
			// LstNameLbl
			// 
			this.LstNameLbl.AutoSize = true;
			this.LstNameLbl.Location = new System.Drawing.Point(7, 89);
			this.LstNameLbl.Name = "LstNameLbl";
			this.LstNameLbl.Size = new System.Drawing.Size(76, 20);
			this.LstNameLbl.TabIndex = 2;
			this.LstNameLbl.Text = "Last name";
			// 
			// FstNameLbl
			// 
			this.FstNameLbl.AutoSize = true;
			this.FstNameLbl.Location = new System.Drawing.Point(7, 133);
			this.FstNameLbl.Name = "FstNameLbl";
			this.FstNameLbl.Size = new System.Drawing.Size(77, 20);
			this.FstNameLbl.TabIndex = 2;
			this.FstNameLbl.Text = "First name";
			// 
			// passportLbl
			// 
			this.passportLbl.AutoSize = true;
			this.passportLbl.Location = new System.Drawing.Point(9, 32);
			this.passportLbl.Name = "passportLbl";
			this.passportLbl.Size = new System.Drawing.Size(87, 20);
			this.passportLbl.TabIndex = 2;
			this.passportLbl.Text = "Passport SN";
			// 
			// FindByNameBtn
			// 
			this.FindByNameBtn.Location = new System.Drawing.Point(333, 126);
			this.FindByNameBtn.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
			this.FindByNameBtn.Name = "FindByNameBtn";
			this.FindByNameBtn.Size = new System.Drawing.Size(85, 33);
			this.FindByNameBtn.TabIndex = 0;
			this.FindByNameBtn.Text = "Find";
			this.FindByNameBtn.UseVisualStyleBackColor = true;
			this.FindByNameBtn.Click += new System.EventHandler(this.FindByNameBtn_Click);
			// 
			// LstNameBox
			// 
			this.LstNameBox.Location = new System.Drawing.Point(103, 85);
			this.LstNameBox.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
			this.LstNameBox.Name = "LstNameBox";
			this.LstNameBox.Size = new System.Drawing.Size(225, 27);
			this.LstNameBox.TabIndex = 1;
			// 
			// FstNameBox
			// 
			this.FstNameBox.Location = new System.Drawing.Point(103, 129);
			this.FstNameBox.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
			this.FstNameBox.Name = "FstNameBox";
			this.FstNameBox.Size = new System.Drawing.Size(225, 27);
			this.FstNameBox.TabIndex = 1;
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.arrivalPortLbl);
			this.groupBox2.Controls.Add(this.flightIdLbl);
			this.groupBox2.Controls.Add(this.arrivalPortBtn);
			this.groupBox2.Controls.Add(this.flightIdBtn);
			this.groupBox2.Controls.Add(this.arrivalPortBox);
			this.groupBox2.Controls.Add(this.flightIdBox);
			this.groupBox2.Location = new System.Drawing.Point(478, 16);
			this.groupBox2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.groupBox2.Size = new System.Drawing.Size(427, 133);
			this.groupBox2.TabIndex = 6;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Searching flight";
			// 
			// arrivalPortLbl
			// 
			this.arrivalPortLbl.AutoSize = true;
			this.arrivalPortLbl.Location = new System.Drawing.Point(7, 90);
			this.arrivalPortLbl.Name = "arrivalPortLbl";
			this.arrivalPortLbl.Size = new System.Drawing.Size(84, 20);
			this.arrivalPortLbl.TabIndex = 2;
			this.arrivalPortLbl.Text = "Arrival port";
			// 
			// flightIdLbl
			// 
			this.flightIdLbl.AutoSize = true;
			this.flightIdLbl.Location = new System.Drawing.Point(9, 33);
			this.flightIdLbl.Name = "flightIdLbl";
			this.flightIdLbl.Size = new System.Drawing.Size(63, 20);
			this.flightIdLbl.TabIndex = 2;
			this.flightIdLbl.Text = "Flight Id";
			// 
			// arrivalPortBtn
			// 
			this.arrivalPortBtn.Location = new System.Drawing.Point(332, 82);
			this.arrivalPortBtn.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
			this.arrivalPortBtn.Name = "arrivalPortBtn";
			this.arrivalPortBtn.Size = new System.Drawing.Size(85, 33);
			this.arrivalPortBtn.TabIndex = 0;
			this.arrivalPortBtn.Text = "Find";
			this.arrivalPortBtn.UseVisualStyleBackColor = true;
			this.arrivalPortBtn.Click += new System.EventHandler(this.arrivalPortBtn_Click);
			// 
			// flightIdBtn
			// 
			this.flightIdBtn.Location = new System.Drawing.Point(333, 25);
			this.flightIdBtn.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
			this.flightIdBtn.Name = "flightIdBtn";
			this.flightIdBtn.Size = new System.Drawing.Size(85, 33);
			this.flightIdBtn.TabIndex = 0;
			this.flightIdBtn.Text = "Find";
			this.flightIdBtn.UseVisualStyleBackColor = true;
			this.flightIdBtn.Click += new System.EventHandler(this.flightIdBtn_Click);
			// 
			// arrivalPortBox
			// 
			this.arrivalPortBox.Location = new System.Drawing.Point(103, 85);
			this.arrivalPortBox.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
			this.arrivalPortBox.Name = "arrivalPortBox";
			this.arrivalPortBox.Size = new System.Drawing.Size(225, 27);
			this.arrivalPortBox.TabIndex = 1;
			// 
			// flightIdBox
			// 
			this.flightIdBox.Location = new System.Drawing.Point(105, 28);
			this.flightIdBox.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
			this.flightIdBox.Name = "flightIdBox";
			this.flightIdBox.Size = new System.Drawing.Size(225, 27);
			this.flightIdBox.TabIndex = 1;
			// 
			// passengerInfoLbl
			// 
			this.passengerInfoLbl.AutoSize = true;
			this.passengerInfoLbl.Location = new System.Drawing.Point(480, 162);
			this.passengerInfoLbl.Name = "passengerInfoLbl";
			this.passengerInfoLbl.Size = new System.Drawing.Size(0, 20);
			this.passengerInfoLbl.TabIndex = 7;
			// 
			// flightInfoLbl
			// 
			this.flightInfoLbl.AutoSize = true;
			this.flightInfoLbl.Location = new System.Drawing.Point(480, 193);
			this.flightInfoLbl.Name = "flightInfoLbl";
			this.flightInfoLbl.Size = new System.Drawing.Size(0, 20);
			this.flightInfoLbl.TabIndex = 8;
			// 
			// sellTicketBtn
			// 
			this.sellTicketBtn.Location = new System.Drawing.Point(945, 167);
			this.sellTicketBtn.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
			this.sellTicketBtn.Name = "sellTicketBtn";
			this.sellTicketBtn.Size = new System.Drawing.Size(135, 33);
			this.sellTicketBtn.TabIndex = 3;
			this.sellTicketBtn.Text = "Sell ticket";
			this.sellTicketBtn.UseVisualStyleBackColor = true;
			this.sellTicketBtn.Click += new System.EventHandler(this.sellTicketBtn_Click);
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1107, 592);
			this.Controls.Add(this.sellTicketBtn);
			this.Controls.Add(this.flightInfoLbl);
			this.Controls.Add(this.passengerInfoLbl);
			this.Controls.Add(this.groupBox2);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.flightsDgView);
			this.Controls.Add(this.passengersDgView);
			this.Controls.Add(this.passangerLabel);
			this.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
			this.Name = "Form1";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Form1";
			this.Load += new System.EventHandler(this.Form1_Load);
			((System.ComponentModel.ISupportInitialize)(this.passengersDgView)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.flightsDgView)).EndInit();
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.groupBox2.ResumeLayout(false);
			this.groupBox2.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button FindByPassport;
		private System.Windows.Forms.TextBox passportNumberBox;
		private System.Windows.Forms.Label passangerLabel;
		private System.Windows.Forms.DataGridView passengersDgView;
		private System.Windows.Forms.DataGridView flightsDgView;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Label LstNameLbl;
		private System.Windows.Forms.Label FstNameLbl;
		private System.Windows.Forms.Label passportLbl;
		private System.Windows.Forms.Button FindByNameBtn;
		private System.Windows.Forms.TextBox LstNameBox;
		private System.Windows.Forms.TextBox FstNameBox;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.Label arrivalPortLbl;
		private System.Windows.Forms.Label flightIdLbl;
		private System.Windows.Forms.Button flightIdBtn;
		private System.Windows.Forms.TextBox arrivalPortBox;
		private System.Windows.Forms.TextBox flightIdBox;
		private System.Windows.Forms.Button arrivalPortBtn;
		private System.Windows.Forms.Label passengerInfoLbl;
		private System.Windows.Forms.Label flightInfoLbl;
		private System.Windows.Forms.Button sellTicketBtn;
	}
}
