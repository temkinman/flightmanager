﻿using FlightManager.Lib.Core;
using FlightManager.Lib.Model;
using System;
using System.Collections.Generic;
using System.Windows.Forms;
using FlightManager.UI.Services;

namespace FlightManager.UI
{
	public partial class Form1 : Form
	{
		private readonly PassengerManager _passengerManager = new PassengerManager();
		private readonly SimpleFlightManager _simpleFlightManager = new SimpleFlightManager();
		private readonly TicketSaleManager _ticketSaleManager = new TicketSaleManager();
		private List<Passenger> _passengers;
		private List<Flight> _flights;
		private Passenger _foundPassenger;
		private Flight _foundFlight;

		public Form1()
		{
			InitializeComponent();
			FillData();
		}

		private void FillData()
		{
			foreach (var passenger in DataFiller.CreatePassengers())
			{
				_passengerManager.Create(passenger);
			}

			foreach (var flight in DataFiller.CreateFlights())
			{
				_simpleFlightManager.Create(flight);
			}

			string[] passengerColumns = { "Last name", "First name", "Passport", "Birth date" };
			int[] passengerColumnsWidths = { 100, 100, 120, 100 };
			DataService.CreateColumnHeaders(passengerColumns, passengerColumnsWidths, passengersDgView);

			string[] flightColumns = { "FlightId", "Airline", "Arrival port", "Departure port", "Departure time", "Seats" };
			int[] flightColumnsWidths = { 90, 90, 80, 80, 120, 60 };
			DataService.CreateColumnHeaders(flightColumns, flightColumnsWidths, flightsDgView);

			_passengers = _passengerManager.GetAll();
			_passengers.ForEach(p => DataService.AddItemToGridView(p, passengersDgView));

			_flights = _simpleFlightManager.GetAll();
			_flights.ForEach(p => DataService.AddItemToGridView(p, flightsDgView));

		}


		private void button1_Click(object sender, EventArgs e)
		{
			string passportNumber = passportNumberBox.Text;
			_foundPassenger = null;

			_foundPassenger = _passengerManager.Find(passportNumber);

			if (_foundPassenger != null)
			{
				int rowIndex = DataService.FindRecord(_foundPassenger, passengersDgView, SearchFields.PASSPORT);
				DataService.SelectFoundRowInGrid(passengersDgView, rowIndex);

				passengerInfoLbl.Text = "Passenger info: " + _foundPassenger.ToString();
			}
			else
			{
				passengersDgView.ClearSelection();
				passengerInfoLbl.Text = "";

				MessageBox.Show("Passenger is not found", "searching passenger", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
		}

		private void Form1_Load(object sender, EventArgs e)
		{
			passengersDgView.ClearSelection();
			flightsDgView.ClearSelection();
		}

		private void FindByNameBtn_Click(object sender, EventArgs e)
		{
			string lastName = LstNameBox.Text;
			string firstName = FstNameBox.Text;

			if (string.IsNullOrWhiteSpace(lastName) ||
				string.IsNullOrWhiteSpace(firstName))
			{
				MessageBox.Show("Please fill in the last name and the first name", "searching passenger", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}

			_foundPassenger = null;
			_foundPassenger = _passengerManager.Find(firstName, lastName);

			if (_foundPassenger != null)
			{
				int rowIndex = DataService.FindRecord(_foundPassenger, passengersDgView, SearchFields.FIO);
				DataService.SelectFoundRowInGrid(passengersDgView, rowIndex);

				passengerInfoLbl.Text = "Passenger info: " + _foundPassenger.ToString();
			}
			else
			{
				passengersDgView.ClearSelection();
				passengerInfoLbl.Text = "";
				MessageBox.Show("Passenger is not found", "searching passenger", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
		}

		private void flightIdBtn_Click(object sender, EventArgs e)
		{
			if (string.IsNullOrWhiteSpace(flightIdBox.Text))
			{
				MessageBox.Show("Please fill in flight id", "searching flight", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}

			Guid flightId = Guid.Empty;
			try
			{
				flightId = Guid.Parse(flightIdBox.Text);
			}
			catch (Exception)
			{
				MessageBox.Show($"Your flight id '{flightIdBox.Text}' is wrong", "searching flight", MessageBoxButtons.OK, MessageBoxIcon.Error);
				return;
			}

			_foundFlight = null;
			_foundFlight = _simpleFlightManager.Find(flightId);

			if (_foundFlight != null)
			{
				int rowIndex = DataService.FindRecord(_foundFlight, flightsDgView, SearchFields.FLIGHTNUMBER);
				DataService.SelectFoundRowInGrid(flightsDgView, rowIndex);

				flightInfoLbl.Text = "Flight info: " + _foundFlight.ToString();
			}
			else
			{
				flightsDgView.ClearSelection();
				flightInfoLbl.Text = "";
				MessageBox.Show("Flight is not found", "searching flight", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
		}

		private void arrivalPortBtn_Click(object sender, EventArgs e)
		{
			string arrivalPort = arrivalPortBox.Text;

			if (string.IsNullOrWhiteSpace(arrivalPort))
			{
				MessageBox.Show("Please fill in arrival port", "searching flight", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}

			_foundFlight = null;
			_foundFlight = _simpleFlightManager.Find(arrivalPort);

			if (_foundFlight != null)
			{
				int rowIndex = DataService.FindRecord(_foundFlight, flightsDgView, SearchFields.ARRIVALPORT);
				DataService.SelectFoundRowInGrid(flightsDgView, rowIndex);

				flightInfoLbl.Text = "Flight info: " + _foundFlight.ToString();
			}
			else
			{
				flightsDgView.ClearSelection();
				flightInfoLbl.Text = "";
				MessageBox.Show("Flight is not found", "searching flight", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
		}

		private void sellTicketBtn_Click(object sender, EventArgs e)
		{
			if (_foundPassenger == null)
			{
				MessageBox.Show("Passenger is not found", "searching passenger", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}

			if (_foundFlight == null)
			{
				MessageBox.Show("Flight is not found", "searching flight", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}

			_ticketSaleManager.SellTicket(_foundFlight, _foundPassenger);
			MessageBox.Show("The ticket was sold.", "tickets", MessageBoxButtons.OK, MessageBoxIcon.Information);
		}
	}
}
