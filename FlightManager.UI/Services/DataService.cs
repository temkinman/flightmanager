﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FlightManager.Lib.Model;

namespace FlightManager.UI.Services
{
	public static class DataService
	{
		public static void AddItemToGridView<T>(T obj, DataGridView dgView)
		{
			string[] info = Array.Empty<string>();

			switch (obj)
			{
				case Passenger passenger:
					{
						info = new[] { passenger.LastName, passenger.FirstName, passenger.PassportNumber, passenger.BirthDate.ToString("dd/MM/yyyy") };
						break;
					}
				case Flight flight:
					{
						info = new[] { flight.FlightNumber.ToString(), flight.AirLineName, flight.ArrivalPort, flight.DeparturePort, flight.DepartureTime.ToString("dd/MM/yyyy HH:mm"), flight.SeatsNumber.ToString() };
						break;
					}
			}

			dgView.Rows.Add(info);
		}

		public static void CreateColumnHeaders(string[] headers, int[] columnWidths, DataGridView dgView)
		{
			dgView.Columns.Clear();
			dgView.Rows.Clear();

			for (int i = 0; i < headers.Length; i++)
			{
				dgView.Columns.Add(headers[i], headers[i]);
				dgView.Columns[i].Width = columnWidths[i];
			}
		}

		public static int FindRecord<T>(T obj, DataGridView dgView, SearchFields nameField)
		{
			int rowIndex = -1;

			string lastName = string.Empty;
			string firstName = string.Empty;
			string passportNumber = string.Empty;

			Guid flightGuid = Guid.Empty;
			string arrivalPort = string.Empty;

			switch (nameField)
			{
				case SearchFields.FIO:
					{
						Passenger passenger = obj as Passenger;
						lastName = passenger?.LastName;
						firstName = passenger?.FirstName;
						break;
					}
				case SearchFields.PASSPORT:
					{
						Passenger passenger = obj as Passenger;
						passportNumber = passenger?.PassportNumber;
						break;
					}
				case SearchFields.ARRIVALPORT:
					{
						Flight flight = obj as Flight;
						arrivalPort = flight?.ArrivalPort;
						break;
					}
				case SearchFields.FLIGHTNUMBER:
					{
						Flight flight = obj as Flight;
						flightGuid = flight.FlightNumber;
						break;
					}
			}

			foreach (DataGridViewRow row in dgView.Rows)
			{
				switch (nameField)
				{
					case SearchFields.FIO:
						{
							if (
								row.Cells[0].Value.ToString().Equals(lastName) &&
								row.Cells[1].Value.ToString().Equals(firstName)
								)
							{
								rowIndex = row.Index;
							}
							break;
						}
					case SearchFields.PASSPORT:
						{
							if (row.Cells[2].Value.ToString().Equals(passportNumber))
							{
								rowIndex = row.Index;
							}
							break;
						}
					case SearchFields.ARRIVALPORT:
						{
							if (row.Cells[2].Value.ToString().Equals(arrivalPort))
							{
								rowIndex = row.Index;
							}
							break;
						}

					case SearchFields.FLIGHTNUMBER:
						{
							Guid foundedId = Guid.Parse(row.Cells[0].Value.ToString());

							if (foundedId.CompareTo(flightGuid) == 0)
							{
								rowIndex = row.Index;
							}
							break;
						}
				}

				if (rowIndex != -1)
				{
					return rowIndex;
				}
			}

			return rowIndex;
		}

		public static void SelectFoundRowInGrid(DataGridView dgView, int rowIndex)
		{
			if (rowIndex != -1)
			{
				dgView.ClearSelection();
				dgView.Rows[rowIndex].Selected = true;
			}
		}
	}
}
