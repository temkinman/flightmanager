﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlightManager.UI.Services
{
	public enum SearchFields
	{
		FIO,
		PASSPORT,
		ARRIVALPORT,
		FLIGHTNUMBER
	}
}
